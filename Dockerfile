FROM adoptopenjdk/openjdk11:alpine-jre
ARG ARTIFACT_ID
ARG BUILD_VERSION

RUN mkdir /app
RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
COPY ./target/$ARTIFACT_ID-$BUILD_VERSION.jar /app/java-app.jar

WORKDIR /app
RUN chown -R javauser:javauser /app
USER javauser

ENTRYPOINT ["java", "-jar", "/app/java-app.jar"]