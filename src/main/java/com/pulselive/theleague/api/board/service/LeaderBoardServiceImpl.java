package com.pulselive.theleague.api.board.service;

import com.pulselive.demo.LeagueTable;
import com.pulselive.demo.LeagueTableEntry;
import com.pulselive.demo.Match;
import com.pulselive.theleague.api.board.web.mapper.MatchMapper;
import com.pulselive.theleague.api.board.web.model.MatchDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
@RequiredArgsConstructor
public class LeaderBoardServiceImpl implements LeaderBoardService {

    private final MatchMapper mapper;

    @Override
    public List<LeagueTableEntry> getLeagueTable(final List<MatchDto> matches) {
        List<Match> matchesToBeProcessed = matches
                .stream()
                .map(mapper::matchDtoToMatch)
                .collect(Collectors.toList());

        LeagueTable league = new LeagueTable(matchesToBeProcessed);
        return league.getTableEntries();
    }
}
