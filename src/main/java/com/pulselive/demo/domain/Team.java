package com.pulselive.demo.domain;

import com.pulselive.demo.domain.vo.MatchEvent;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Team {
    private final String name;
    private final List<MatchEvent> events = new ArrayList<>();

    public Team(final String name) {
        this.name = name;
    }

    public void processMatch(MatchEvent matchEvent) {
        events.add(matchEvent);
    }

    public int lost() {
        return (int) events.stream().filter(MatchEvent::lost).count();
    }

    public int won() {
        return (int) events.stream().filter(MatchEvent::won).count();
    }

    public int drawn() {
        return (int) events.stream().filter(MatchEvent::drawn).count();
    }

    public int played() {
        return events.size();
    }

    public int points() {
        return events.stream().mapToInt(MatchEvent::getPoints).sum();
    }

    public int goalsFor() {
        return this.events.stream().mapToInt(MatchEvent::getFthg).sum();
    }

    public int goalsAgainst() {
        return this.events.stream().mapToInt(MatchEvent::getFtga).sum();
    }

    public int goalsDiff() {
        return goalsFor() - goalsAgainst();
    }
}
