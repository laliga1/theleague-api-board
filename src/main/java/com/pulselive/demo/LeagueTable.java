package com.pulselive.demo;

import com.pulselive.demo.comparator.LeagueTableEntryComparator;
import com.pulselive.demo.domain.Team;
import com.pulselive.demo.domain.vo.MatchEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LeagueTable {

    private final Map<String, Team> teams = new HashMap<>();

    private LeagueTable() {
        // protected constructor
    }

    public LeagueTable(final List<Match> matches) {
        if (matches != null && !matches.isEmpty()) {
            processMatches(matches);
        }
    }

    public List<LeagueTableEntry> getTableEntries() {
        if (teams.isEmpty()) {
            return Collections.emptyList();
        }

        return teams.values().stream()
                .map(this::teamsToLeagueTableEntry)
                .sorted(LeagueTableEntryComparator.builder()
                        .sortByPoints()
                        .sortByGoalsDiff()
                        .sortByGoalsFor()
                        .sortByTeamName().build())
                .collect(Collectors.toList());
    }

    private void processMatches(List<Match> matches) {
        matches.forEach(this::processMatch);
    }

    private void processMatch(Match m) {
        Team homeTeam = getTeam(m.getHomeTeam());
        Team awayTeam = getTeam(m.getAwayTeam());
        homeTeam.processMatch(new MatchEvent(m.getHomeScore(), m.getAwayScore()));
        awayTeam.processMatch(new MatchEvent(m.getAwayScore(), m.getHomeScore()));
    }

    private Team getTeam(String teamName) {
        Team team = teams.get(teamName);
        if (team == null) {
            team = new Team(teamName);
            teams.put(teamName, team);
        }
        return team;
    }

    private LeagueTableEntry teamsToLeagueTableEntry(Team t) {
        return LeagueTableEntry.builder()
                .teamName(t.getName())
                .lost(t.lost())
                .won(t.won())
                .drawn(t.drawn())
                .played(t.played())
                .points(t.points())
                .goalsFor(t.goalsFor())
                .goalsAgainst(t.goalsAgainst())
                .goalDifference(t.goalsDiff())
                .build();
    }
}
