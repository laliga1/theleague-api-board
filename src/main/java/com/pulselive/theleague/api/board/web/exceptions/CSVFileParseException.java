package com.pulselive.theleague.api.board.web.exceptions;

public class CSVFileParseException extends RuntimeException {
    public CSVFileParseException(String message) {
        super(message);
    }
}
