package com.pulselive.theleague.api.board.web.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class MatchDto {

    @CsvBindByName(column = "HomeTeam")
    @NotEmpty(message = "'HomeTeam' can not be null or empty")
    private String homeTeam;

    @CsvBindByName(column = "AwayTeam")
    @NotEmpty(message = "'AwayTeam' can not be null or empty")
    @NotEmpty
    private String awayTeam;

    @CsvBindByName(column = "FTHG")
    @Min(value = 0, message = "'FTHG' con not be negative")
    private int homeScore;

    @CsvBindByName(column = "FTAG")
    @Min(value = 0, message = "'FTAG' con not be negative")
    private int awayScore;
}
