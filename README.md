# Leader Board Challenge 

## Challenge Basic Solution

### Solution Class diagram
#### Approach 1 (first approach)

Branch: [first_approach](https://gitlab.com/laliga1/theleague-api-board/-/tree/first_approach) 

![Design.LeagueBoard.Approach1](/uploads/cf2607c368702a81e2a9d12163716745/Design.LeagueBoard.Approach1.png)

#### Approach 2 (cleaner design)
Inner classes 
![Design.LeagueBoard.Approach2](/uploads/3ed7125b6d4bf38268a67567d391d59b/Design.LeagueBoard.Approach2.png)

Inner classes hidden
![Design.LeagueBoard.Approach2.NoInnerClasses](/uploads/eb9f708f79cea286c2e244f623b22178/Design.LeagueBoard.Approach2.NoInnerClasses.png)

### Testing the solution
	
Two test classes have been implemented:
* _[LeagueTableEntrySorterTest](https://gitlab.com/laliga1/theleague-api-board/-/blob/dev/src/test/java/com/pulselive/demo/LeagueTableEntrySorterTest.java)_: tests the custom _Comparator<T>_
* _[LeagueTableTest.class](https://gitlab.com/laliga1/theleague-api-board/-/blob/dev/src/test/java/com/pulselive/demo/LeagueTableTest.java)_: tests the algorithm to sort matches received by a _LeagueTable_ object

[Test data](https://gitlab.com/laliga1/theleague-api-board/-/tree/dev/src/test/resources/test-data) has been gathered from [Datahub.io](https://datahub.io/), and specifically from the [Spanish Football League](https://datahub.io/).

To execute the test:
  ```console
  mvn test -Dtest=LeagueTableEntrySorterTest,LeagueTableTest 
  ```


## Challenge Rest API wrapper

In order to make the sorting algorithm publicly available, a Rest API wrapper has been developed. 

The end point **/api/v1/board** accepts csv files that must include, at least, the following fields:
* **_HomeTeam_**: it'll be mapped to _Match.homeTeam_
* **_AwayTeam_**: it'll be mapped to _Match.awayTeam_
* **_FTHG_**: it'll be mapped to _Match.homeScore_
* **_FTAG_**: it'll be mapped to _Match.awayScore_

To execute the unit tests:

  ```console
   mvn test -Dspring.profiles.active=unit-tests 
  ```

To make the end point available in localhost:

  ```console
   mvn spring-boot:run 
  ```