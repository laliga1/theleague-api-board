package com.pulselive.theleague.api.board.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class CorsConfig {

    private final CorsConfigProperties corsConfig;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        List<String> allowedOrigins = corsConfig.getAllowedOrigins();
        List<String> allowedMethods = corsConfig.getAllowedMethods();

        log.info("Cors configuration: Allowed Origins -> {} | Allowed Methods -> {}", allowedOrigins, allowedMethods);
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                if (!CollectionUtils.isEmpty(allowedOrigins)) {
                    registry.addMapping("/**")
                            .allowedOrigins(allowedOrigins.toArray(String[]::new));
                }

                if (!CollectionUtils.isEmpty(allowedMethods)) {
                    registry.addMapping("/**")
                            .allowedMethods(allowedMethods.toArray(String[]::new));
                }
            }
        };
    }
}
