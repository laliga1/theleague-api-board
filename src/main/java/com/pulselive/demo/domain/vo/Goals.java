package com.pulselive.demo.domain.vo;

import lombok.Value;

@Value
class Goals {
    int value;
    Goals(int value) {
        if (value < 0) {
            throw new InvalidGoalsException("Score can not have a negative value");
        }
        this.value = value;
    }
}
