package com.pulselive.theleague.api.board.web.controller;

import com.pulselive.demo.LeagueTableEntry;
import com.pulselive.theleague.api.board.service.LeaderBoardService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.anyList;
import static org.mockito.BDDMockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.MethodName.class)
@AutoConfigureJsonTesters
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(value = LeaderBoardController.class)
@Tags({@Tag(value = "web")})
@EnabledIf(
        expression = "#{environment.acceptsProfiles('unit-tests')}",
        loadContext = true)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LeaderBoardControllerTest {

    private static final String DEFAULT_CSV_FILE_PATH = "laliga/";

    @MockBean
    private LeaderBoardService service;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ResourceLoader resourceLoader;

    @Test
    void getLeaderBoardWhenFileIsNotInTheRightParamKey() throws Exception {
        // given
        String fileName = "laliga-season-1617.csv";
        String contentType = "text/csv";
        byte[] fileToStored = readFile(fileName);
        String fileKey = "wrong-file-key";
        MockMultipartFile file = new MockMultipartFile(fileKey, fileName, contentType, fileToStored);

        // when
        mockMvc
                .perform(MockMvcRequestBuilders.multipart("/api/v1/board")
                        .file(file))

                // then
                .andExpect(status().isBadRequest());
    }

    @Test
    void getLeaderBoardWhenHappyPath() throws Exception {
        // given
        String fileName = "laliga-season-1617.csv";
        String contentType = "text/csv";
        byte[] fileToStored = readFile(fileName);
        String fileKey = "csv";
        MockMultipartFile file = new MockMultipartFile(fileKey, fileName, contentType, fileToStored);
        when(service.getLeagueTable(anyList())).thenReturn(List.of(LeagueTableEntry.builder()
                .teamName("Real Madrid")
                .build()));

        // when
        mockMvc
                .perform(MockMvcRequestBuilders.multipart("/api/v1/board")
                        .file(file))

                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].teamName", containsInAnyOrder("Real Madrid")));
    }

    private byte[] readFile(String filename) throws IOException {
        File file = resourceLoader
                .getResource("classpath:" + DEFAULT_CSV_FILE_PATH + filename)
                .getFile();
        return Files.readAllBytes(file.toPath());
    }

}