package com.pulselive.theleague.api.board.service;

import com.pulselive.demo.LeagueTableEntry;
import com.pulselive.theleague.api.board.web.model.MatchDto;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface LeaderBoardService {

    List<LeagueTableEntry> getLeagueTable(@NotEmpty final List<MatchDto> matches);
}
