package com.pulselive.theleague.api.board.web.mapper;

import com.pulselive.demo.Match;
import com.pulselive.theleague.api.board.web.model.MatchDto;
import org.mapstruct.Mapper;

@Mapper
public interface MatchMapper {

    MatchDto matchToMatchDto(Match match);

    Match matchDtoToMatch(MatchDto dto);
}
