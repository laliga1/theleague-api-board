package com.pulselive.demo;

import com.pulselive.common.JacksonObjectMapperUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class LeagueTableTest {

    private static final String DEFAULT_TEST_DATA_FILE = "matches-test-data.json";
    private static final String LA_LIGA_2016_2017 = "laliga-season-1617.json";
    private static final String LA_LIGA_2017_2018 = "laliga-season-1718.json";
    private static final String LA_LIGA_2018_2019 = "laliga-season-1819.json";

    private LeagueTable cut;

    @Test
    void getTableEntriesWhenNoMatchesShouldReturnEmptyList() {
        cut = new LeagueTable(null);

        List<LeagueTableEntry> leaderBoard = cut.getTableEntries();

        Assertions.assertNotNull(leaderBoard);
        Assertions.assertTrue(leaderBoard.isEmpty(), "Leader board must be empty");
    }

    @Test
    void getTableEntriesWhenRealMadridIsLeader() {
        LeagueTableEntry expectedLeaderEntry = LeagueTableEntry.builder()
                .teamName("Real Madrid")
                .played(2)
                .points(6)
                .won(2)
                .lost(0)
                .drawn(0)
                .goalsFor(6)
                .goalsAgainst(0)
                .goalDifference(6)
                .build();
        Match[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                DEFAULT_TEST_DATA_FILE, Match[].class);
        cut = new LeagueTable(List.of(entries));

        List<LeagueTableEntry> leaderBoard = cut.getTableEntries();

        assertLeader(expectedLeaderEntry, leaderBoard);
    }

    @Test
    void getTableEntriesWhenSeason1617() {
        LeagueTableEntry expectedLeaderEntry = LeagueTableEntry.builder()
                .teamName("Real Madrid")
                .played(38)
                .points(93)
                .won(29)
                .lost(3)
                .drawn(6)
                .goalsFor(106)
                .goalsAgainst(41)
                .goalDifference(65)
                .build();
        Match[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                LA_LIGA_2016_2017, Match[].class);
        cut = new LeagueTable(List.of(entries));
        List<LeagueTableEntry> leaderBoard = cut.getTableEntries();

        assertLeader(expectedLeaderEntry, leaderBoard);
    }

    @Test
    void getTableEntriesWhenSeason1718() {
        LeagueTableEntry expectedLeaderEntry = LeagueTableEntry.builder()
                .teamName("Barcelona")
                .played(38)
                .points(93)
                .won(28)
                .lost(1)
                .drawn(9)
                .goalsFor(99)
                .goalsAgainst(29)
                .goalDifference(70)
                .build();
        Match[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                LA_LIGA_2017_2018, Match[].class);
        cut = new LeagueTable(List.of(entries));
        List<LeagueTableEntry> leaderBoard = cut.getTableEntries();

        assertLeader(expectedLeaderEntry, leaderBoard);
    }

    @Test
    void getTableEntriesWhenSeason1819() {
        LeagueTableEntry expectedLeaderEntry = LeagueTableEntry.builder()
                .teamName("Barcelona")
                .played(38)
                .points(87)
                .won(26)
                .lost(3)
                .drawn(9)
                .goalsFor(90)
                .goalsAgainst(36)
                .goalDifference(54)
                .build();
        Match[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                LA_LIGA_2018_2019, Match[].class);
        cut = new LeagueTable(List.of(entries));
        List<LeagueTableEntry> leaderBoard = cut.getTableEntries();

        assertLeader(expectedLeaderEntry, leaderBoard);
    }

    private void assertLeader(LeagueTableEntry expectedLeaderEntry, List<LeagueTableEntry> leaderBoard) {
        Assertions.assertNotNull(leaderBoard);
        Assertions.assertFalse(leaderBoard.isEmpty(), "Leader board can not be empty");
        Assertions.assertEquals(
                expectedLeaderEntry,
                leaderBoard.get(0),
                String.format("Leader must be %s", expectedLeaderEntry.getTeamName()));
    }
}