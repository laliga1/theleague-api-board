package com.pulselive.demo.domain.vo;

class InvalidGoalsException extends RuntimeException {
    InvalidGoalsException(String message) {
        super(message);
    }
}
