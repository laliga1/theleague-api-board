package com.pulselive.demo.domain.vo;

enum FTR {

    LOST(0, "A"),
    WON(3, "H"),
    DRAWN(1, "D");

    private final int points;
    private final String code;

    FTR(final int points, final String code) {
        this.points = points;
        this.code = code;
    }

    public int getPoints() {
        return this.points;
    }

    public String getCode() {
        return this.code;
    }
}
