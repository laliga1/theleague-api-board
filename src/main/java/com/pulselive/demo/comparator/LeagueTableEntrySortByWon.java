package com.pulselive.demo.comparator;

import com.pulselive.demo.LeagueTableEntry;

import java.util.Comparator;

public class LeagueTableEntrySortByWon implements Comparator<LeagueTableEntry> {

    @Override
    public int compare(LeagueTableEntry e1, LeagueTableEntry e2) {
        return e2.getWon() - e1.getWon();
    }
}
