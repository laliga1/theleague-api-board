package com.pulselive;

import com.pulselive.common.JacksonObjectMapperUtils;
import com.pulselive.demo.Match;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MatchCVS2JsonConverter {
    private static final String SOURCE_RELATIVE_PATH = "laliga/";
    private static final String DESTINATION_RELATIVE_PATH = "./";
    private static final String FILE_EXTENSION_PATTERN = "[.][^.]+$";
    private static final Pattern CSV_SEPARATOR_PATTERN = Pattern.compile(",");

    public static void main(String[] args) throws Exception {

        MatchCVS2JsonConverter converter = new MatchCVS2JsonConverter();

        List<String> filesToConvert = converter.getNotExistingFileNames(SOURCE_RELATIVE_PATH, DESTINATION_RELATIVE_PATH);
        filesToConvert.forEach(f -> {
            try {
                converter.convertFile(SOURCE_RELATIVE_PATH, DESTINATION_RELATIVE_PATH, f);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private List<String> getNotExistingFileNames(String sourceFolder, String destinationFolder)  throws Exception {
        List<String> sourceFileNames = getFileNamesFromFolder(sourceFolder);
        List<String> destinationExistingFiles = getFileNamesFromFolder(destinationFolder);
        if (destinationExistingFiles.size() > 0) {
            return sourceFileNames.stream()
                    .filter(s -> !destinationExistingFiles.contains(s))
                    .collect(Collectors.toList());
        }
        return sourceFileNames;
    }

    private List<String> getFileNamesFromFolder(String folder) throws Exception {
        File sourceFolder = new ClassPathResource(folder).getFile();
        if (sourceFolder.exists() && sourceFolder.isDirectory()) {
            File[] files = sourceFolder.listFiles();
            if (null != files && files.length > 0) {
                return Stream.of(files)
                        .filter(File::isFile)
                        .map(f -> f.getName().replaceFirst(FILE_EXTENSION_PATTERN, ""))
                        .collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    private void convertFile(String sourceFolder, String destinationFolder, String filename) throws Exception {
        File csvFile = new ClassPathResource(sourceFolder + filename + ".csv").getFile();
        BufferedReader in = new BufferedReader(new FileReader(csvFile));
        List<Match> matches = in .lines() .skip(1) .map(line -> {
            String[] values = CSV_SEPARATOR_PATTERN.split(line);
            return newMatchFromCSVLine(values);
        }).collect(Collectors.toList());

        String json = JacksonObjectMapperUtils.asJsonString(matches);
        String fileName = destinationFolder + filename + ".json";
        Path path = Paths.get(fileName);

        // default, create, truncate and write to it.
        try (BufferedWriter writer =
                     Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {

            writer.write(json);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Match newMatchFromCSVLine(String[] lineValues) {
        Match match = new Match();
        match.setHomeTeam(lineValues[2]);
        match.setAwayTeam(lineValues[3]);
        match.setHomeScore(Integer.parseInt(lineValues[4]));
        match.setAwayScore(Integer.parseInt(lineValues[5]));
        return match;
    }
}
