package com.pulselive.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.core.io.ClassPathResource;

import java.io.File;

public class JacksonObjectMapperUtils {

    private static final String TEST_DATA_RELATIVE_PATH = "test-data/";

    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper mapper = JsonMapper.builder().build();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T asObjectFromString(final String json, final Class<T> type) {
        try {
            ObjectMapper mapper = JsonMapper.builder().build();
            return mapper.readValue(json, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T asObjectFromFile(final String filename, final Class<T> type) {
        try {
            ObjectMapper mapper = JsonMapper.builder().build();
            File resource = new ClassPathResource(TEST_DATA_RELATIVE_PATH + filename).getFile();
            return mapper.readValue(resource, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
