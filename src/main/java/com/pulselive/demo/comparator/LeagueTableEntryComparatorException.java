package com.pulselive.demo.comparator;

public class LeagueTableEntryComparatorException extends RuntimeException {
    LeagueTableEntryComparatorException(String message) {
        super(message);
    }
}
