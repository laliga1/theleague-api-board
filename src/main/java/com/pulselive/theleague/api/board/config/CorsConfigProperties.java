package com.pulselive.theleague.api.board.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app.security.cors")
public class CorsConfigProperties {

    private List<String> allowedMethods;
    private List<String> allowedOrigins;
}
