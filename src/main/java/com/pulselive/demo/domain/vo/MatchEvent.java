package com.pulselive.demo.domain.vo;

import lombok.Value;

@Value
public class MatchEvent {

    Goals fthg;
    Goals ftga;
    FTR ftr;

    public MatchEvent(int homeScore, int awayScore) {
        this.fthg = new Goals(homeScore);
        this.ftga = new Goals(awayScore);
        boolean drawn = fthg.getValue() == ftga.getValue();
        boolean won = fthg.getValue() > ftga.getValue();
        this.ftr = drawn ? FTR.DRAWN : won ? FTR.WON : FTR.LOST;
    }

    public int getFthg() {
        return this.fthg.getValue();
    }

    public int getFtga() {
        return this.ftga.getValue();
    }

    public int getPoints() {
        return ftr.getPoints();
    }

    public boolean won() {
        return ftr.equals(FTR.WON);
    }

    public boolean lost() {
        return ftr.equals(FTR.LOST);
    }

    public boolean drawn() {
        return ftr.equals(FTR.DRAWN);
    }
}
