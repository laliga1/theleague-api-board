package com.pulselive.theleague.api.board.service;

import com.pulselive.demo.LeagueTableEntry;
import com.pulselive.theleague.api.board.web.model.MatchDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.List;

@TestMethodOrder(MethodOrderer.MethodName.class)
@Tags(value = {@Tag(value = "service")})
@EnabledIf(
        expression = "#{environment.acceptsProfiles('unit-tests')}",
        loadContext = true)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LeaderBoardServiceTest {

    @Autowired
    LeaderBoardService cut;

    @Test
    void getLeagueTableWhenListOfMatchesIsEmpty() {
        Assertions.assertThrows(ConstraintViolationException.class,
                () -> cut.getLeagueTable(Collections.emptyList()),
                "Must throw ConstraintViolationException");
    }

    @Test
    void getLeagueTableWhenHappyPath() {
        MatchDto match = defaultMatch();
        List<MatchDto> matches = List.of(match);

        List<LeagueTableEntry> result = cut.getLeagueTable(matches);

        Assertions.assertNotNull(result, "Board can not be null");
        Assertions.assertFalse(result.isEmpty(), "Board can not be empty");
        Assertions.assertEquals(2, result.size(), String.format("Board size must be %d", 2));
        Assertions.assertEquals(match.getAwayTeam(), result.get(0).getTeamName(), String.format("Leader must be %s", match.getAwayTeam()));
    }

    @Test
    void getLeagueTableWhenCalledTwiceWithDifferentMatches() {
        MatchDto matchBoard1 = defaultMatch();
        MatchDto matchBoard2 = newMatch("Sevilla", 0, "Valencia", 1);

        cut.getLeagueTable(List.of(matchBoard1));
        List<LeagueTableEntry> result = cut.getLeagueTable(List.of(matchBoard2));

        Assertions.assertNotNull(result, "Board can not be null");
        Assertions.assertFalse(result.isEmpty(), "Board can not be empty");
        Assertions.assertEquals(2, result.size(), String.format("Board size must be %d", 2));
    }

    private MatchDto defaultMatch() {
        return newMatch("Barcelona", 0, "Real Madrid", 10);
    }

    private MatchDto newMatch(String homeTeam, int homeScore, String awayTeam, int awayScore) {
        return MatchDto.builder()
                .homeTeam(homeTeam)
                .homeScore(homeScore)
                .awayTeam(awayTeam)
                .awayScore(awayScore)
                .build();
    }

}