package com.pulselive.demo.comparator;

import com.pulselive.demo.LeagueTableEntry;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LeagueTableEntryComparator implements Comparator<LeagueTableEntry> {

    private final List<Comparator<LeagueTableEntry>> comparators;

    private LeagueTableEntryComparator(LeagueTableEntryComparatorBuilder builder) {
        comparators = builder.comparators;
    }

    public static LeagueTableEntryComparatorBuilder builder() {
        return new LeagueTableEntryComparatorBuilder();
    }

    @Override
    public int compare(LeagueTableEntry e1, LeagueTableEntry e2) {
        Comparator<LeagueTableEntry> comparator = comparators.stream().reduce((c1, c2) -> 0, Comparator::thenComparing);
        return comparator.compare(e1, e2);
    }

    public static class LeagueTableEntryComparatorBuilder {

        private final List<Comparator<LeagueTableEntry>> comparators = new ArrayList<>();

        public LeagueTableEntryComparatorBuilder sortByPoints() {
            Comparator<LeagueTableEntry> comparator = (e1, e2) -> e2.getPoints() - e1.getPoints();
            comparators.add(comparator);
            return this;
        }

        public LeagueTableEntryComparatorBuilder sortByGoalsDiff() {
            Comparator<LeagueTableEntry> comparator = (e1, e2) -> e2.getGoalDifference() - e1.getGoalDifference();
            comparators.add(comparator);
            return this;
        }

        public LeagueTableEntryComparatorBuilder sortByGoalsFor() {
            Comparator<LeagueTableEntry> comparator = (e1, e2) -> e2.getGoalsFor() - e1.getGoalsFor();
            comparators.add(comparator);
            return this;
        }

        public LeagueTableEntryComparatorBuilder sortByTeamName() {
            Comparator<LeagueTableEntry> comparator = Comparator.comparing(LeagueTableEntry::getTeamName);
            comparators.add(comparator);
            return this;
        }

        public LeagueTableEntryComparatorBuilder sortBy(Comparator<LeagueTableEntry> comparator) {
            comparators.add(comparator);
            return this;
        }

        public LeagueTableEntryComparator build() {
            LeagueTableEntryComparator comparator =  new LeagueTableEntryComparator(this);
            validateUserObject(comparator);
            return comparator;
        }
        private void validateUserObject(LeagueTableEntryComparator comparator) {
            if (comparator.comparators.size() == 0) {
                throw new LeagueTableEntryComparatorException("No comparator added");
            }
        }
    }
}
