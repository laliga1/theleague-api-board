package com.pulselive.demo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class Match {

    private String homeTeam;
    private String awayTeam;
    private int homeScore;
    private int awayScore;
}
