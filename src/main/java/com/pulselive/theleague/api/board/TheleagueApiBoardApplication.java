package com.pulselive.theleague.api.board;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheleagueApiBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheleagueApiBoardApplication.class, args);
    }

}
