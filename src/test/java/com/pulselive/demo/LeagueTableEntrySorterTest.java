package com.pulselive.demo;

import com.pulselive.common.JacksonObjectMapperUtils;
import com.pulselive.demo.comparator.LeagueTableEntryComparator;
import com.pulselive.demo.comparator.LeagueTableEntryComparatorException;
import com.pulselive.demo.comparator.LeagueTableEntrySortByWon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class LeagueTableEntrySorterTest {

    private static final String TEST_DATA_FILE = "theleague-table-sorter-test-data.json";

    private LeagueTableEntryComparator cut;

    @BeforeEach
    void beforeEach() {
        cut = LeagueTableEntryComparator.builder()
                .sortByPoints()
                .sortByGoalsDiff()
                .sortByGoalsFor()
                .sortByTeamName().build();
    }

    @Test
    void sortByPoints() {
        String expectedLeader = "Real Madrid";
        String expectedLastTeam = "Sevilla";
        LeagueTableEntry[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                TEST_DATA_FILE, LeagueTableEntry[].class);
        var result = Stream.of(entries).sorted(cut).collect(Collectors.toList());

        LeagueTableEntry leader = result.get(0);
        LeagueTableEntry last = result.get(result.size() - 1);
        Assertions.assertEquals(
                expectedLeader,
                leader.getTeamName(), String.format("Leader must be %s", expectedLeader));

        Assertions.assertEquals(
                expectedLastTeam,
                last.getTeamName(), String.format("Last team must be %s", expectedLeader));
    }

    @Test
    void sortByPointsAndGoalsDiff() {
        String firstTeam = "Atletico";
        String secondTeam = "Celta";

        LeagueTableEntry[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                TEST_DATA_FILE,
                LeagueTableEntry[].class);
        var result = Stream.of(entries).sorted(cut).collect(Collectors.toList());

        int posFirst = getPosition(result, firstTeam);
        int posSecond = getPosition(result, secondTeam);

        Assertions.assertEquals(
                1,
                posFirst,
                String.format("%s must be in the position number (index) %d of the table ", firstTeam, 1));
        Assertions.assertEquals(
                1,
                posSecond - posFirst,
                String.format("%s must be fist in the table", firstTeam));
    }

    @Test
    void sortByPointsAndGoalsDiffAndScored() {
        String firstTeam = "Valencia";
        String secondTeam = "Betis";
        int expectedIndexFirstTeam = 4;

        LeagueTableEntry[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                TEST_DATA_FILE,
                LeagueTableEntry[].class);
        var result = Stream.of(entries).sorted(cut).collect(Collectors.toList());

        int posFirst = getPosition(result, firstTeam);
        int posSecond = getPosition(result, secondTeam);

        Assertions.assertEquals(
                expectedIndexFirstTeam,
                posFirst,
                String.format("%s must be in the position number (index) %d of the table ", firstTeam, expectedIndexFirstTeam));
        Assertions.assertEquals(
                1,
                posSecond - posFirst,
                String.format("%s must be fist in the table", firstTeam));
    }

    @Test
    void sortByPointsAndGoalsDiffAndScoredAndName() {
        String firstTeam = "Deportivo";
        String secondTeam = "Numancia";
        int expectedIndexFirstTeam = 6;

        LeagueTableEntry[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                TEST_DATA_FILE,
                LeagueTableEntry[].class);
        var result = Stream.of(entries).sorted(cut).collect(Collectors.toList());

        int posFirst = getPosition(result, firstTeam);
        int posSecond = getPosition(result, secondTeam);

        Assertions.assertEquals(
                expectedIndexFirstTeam,
                posFirst,
                String.format("%s must be in the position number (index) %d of the table ", firstTeam, expectedIndexFirstTeam));
        Assertions.assertEquals(
                1,
                posSecond - posFirst,
                String.format("%s must be fist in the table", firstTeam));
    }

    @Test
    void sortByWon() {
        String expectedLeader = "Real Madrid";
        String expectedLastTeam = "Barcelona";
        LeagueTableEntry[] entries = JacksonObjectMapperUtils.asObjectFromFile(
                TEST_DATA_FILE, LeagueTableEntry[].class);

        LeagueTableEntryComparator cut = LeagueTableEntryComparator.builder()
                .sortBy(new LeagueTableEntrySortByWon())
                .build();

        var result = Stream.of(entries).sorted(cut).collect(Collectors.toList());

        LeagueTableEntry leader = result.get(0);
        LeagueTableEntry last = result.get(result.size() - 1);
        Assertions.assertEquals(
                expectedLeader,
                leader.getTeamName(), String.format("Leader must be %s", expectedLeader));

        Assertions.assertEquals(
                expectedLastTeam,
                last.getTeamName(), String.format("Last team must be %s", expectedLeader));
    }

    @Test
    void sortByWhenNoComparatorAdded() {
        Assertions.assertThrows(
                LeagueTableEntryComparatorException.class,
                () -> LeagueTableEntryComparator.builder().build(),
                "Must thrown LeagueTableEntryComparatorException");
    }

    private int getPosition(List<LeagueTableEntry> entries, String teamName) {
        return IntStream.range(0, entries.size())
                .filter(i -> entries.get(i).getTeamName().equals(teamName))
                .findFirst()
                .orElse(-1);
    }

}