package com.pulselive.theleague.api.board.web.controller;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.pulselive.demo.LeagueTableEntry;
import com.pulselive.theleague.api.board.service.LeaderBoardService;
import com.pulselive.theleague.api.board.web.exceptions.CSVFileParseException;
import com.pulselive.theleague.api.board.web.model.MatchDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/board")
public class LeaderBoardController {

    private final LeaderBoardService service;

    @PostMapping
    public ResponseEntity<List<LeagueTableEntry>> getBoard(@RequestParam("csv") MultipartFile csvFile) {
        if (!csvFile.isEmpty()) {
            List<MatchDto> matches = readFromCSVFile(csvFile);
            return ResponseEntity.ok(service.getLeagueTable(matches));
        }
        return ResponseEntity.badRequest().build();
    }

    private List<MatchDto> readFromCSVFile(MultipartFile csvFile) {
        try (Reader reader = new BufferedReader(new InputStreamReader(csvFile.getInputStream()))) {

            // create csv bean reader
            CsvToBean<MatchDto> csvToBean = new CsvToBeanBuilder<MatchDto>(reader)
                    .withType(MatchDto.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            // convert `CsvToBean` object to list of matches
            return csvToBean.parse();
        } catch (Exception ex) {
            throw new CSVFileParseException(String.format("Error parsing file: %s", csvFile.getName()));
        }
    }

}
